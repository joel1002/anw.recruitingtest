﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ComposerApp.Controllers;
using Moq;
using ANW.ComposerApp.Models;
using System.Linq;

namespace ComposerApp.Tests
{
    [TestClass]
    public class ComposersControllerTests
    {
        [TestMethod]
        public void Get()
        {
            var composersRepo = new Mock<IComposersRepo>();
            composersRepo.Setup(x => x.GetComposers()).Returns(new[]
            {
                new Composer
                {
                    FirstName = "Joe",
                    LastName = "Langford",
                    Awards = "None",
                    Id = 1,
                    Title = "Hello World"
                }
            });
            var composersController = new ComposersController(composersRepo.Object);
            var composers = composersController.Get();
            Composer composer = composers.Single();
            Assert.AreEqual("Joe", composer.FirstName);
            Assert.AreEqual("Langford", composers.Single().LastName);
        }
    }
}
