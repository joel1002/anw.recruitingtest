using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ANW.ComposerApp.Models;

namespace ComposerApp.Controllers
{
    [Route("api/[controller]")]
    public class ComposersController : Controller
    {
        private readonly IComposersRepo _composerRepo;

        public ComposersController(IComposersRepo composerRepo)
        {
            _composerRepo = composerRepo;
        }

        [HttpGet()]
        public IEnumerable<Composer> Get()
        {
            return _composerRepo.GetComposers();
        }
    }
}
