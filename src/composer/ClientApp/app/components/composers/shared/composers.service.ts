﻿import { Injectable, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { IComposer } from './composer.model';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ComposersService {

    constructor(private http: Http, @Inject('ORIGIN_URL') private originUrl: string) {
    }

    getComposers() {
        return this.http.get(this.originUrl + '/api/Composers')
            .map(res => res.json() as IComposer[]);
    }
}

