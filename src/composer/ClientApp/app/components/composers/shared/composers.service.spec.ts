﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, inject } from '@angular/core/testing';
import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { ComposersService } from './composers.service';
import { IComposer } from './composer.model';

describe('ComposersService', () => {

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                { provide: "ORIGIN_URL", useValue: 'http://example.com' },
                ComposersService,
                { provide: XHRBackend, useClass: MockBackend },
            ]
        });
    });

    describe('getComposers()', () => {

        it('should return composers',
            inject([ComposersService, XHRBackend], (composersService, mockBackend) => {

                const mockResponse = [
                    { firstName: "Test", lastName: "Test", title: "Mr", awards: "None" },
                    { firstName: "Test1", lastName: "Test1", title: "Mr", awards: "None"  },
                ];

                mockBackend.connections.subscribe((connection) => {
                    connection.mockRespond(new Response(new ResponseOptions({
                        body: JSON.stringify(mockResponse)
                    })));
                });

                composersService.getComposers().subscribe((composers) => {
                    expect(composers.length).toBe(2);
                });

            }));
    });
});