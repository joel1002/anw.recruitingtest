﻿export interface IComposer {
    firstName: string;
    lastName: string;
    title: string;
    awards: string;
}