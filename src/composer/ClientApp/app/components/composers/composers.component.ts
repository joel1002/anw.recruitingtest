import { Component, OnInit } from '@angular/core';
import { ComposersService } from './shared/composers.service';
import { IComposer } from './shared/composer.model';

@Component({
    selector: 'composers',
    templateUrl: './composers.component.html',
    styleUrls: ['./composers.component.css'],
    providers: [ComposersService]
})
export class ComposersComponent implements OnInit {
    public composers: IComposer[];

    constructor(private composersService: ComposersService) {

    }

    ngOnInit(): void {
        this.composersService.getComposers()
            .subscribe(composers => {
                console.log(JSON.stringify(composers));
                this.composers = composers;
            });
    }
}