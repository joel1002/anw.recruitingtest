/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { ComposersComponent } from './composers.component';
import { ComposersService } from './shared/composers.service';
import { Observable } from "rxjs/Observable";
import { async, TestBed } from "@angular/core/testing";
import { RouterModule, RouterOutletMap  } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

class MockComposersService {
    getComposers() {
        return Observable.create([
            { firstName: "Test", lastName: "Test", title: "Mr", awards: "None"  },
            { firstName: "Test1", lastName: "Test1", title: "Mr", awards: "None"  },
        ]);
    }
}

describe('Composer Component', () => {
    
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ComposersComponent],
            providers: [
                { provide: ComposersService, useClass: MockComposersService },
                { provide: 'ORIGIN_URL', useValue: 'Test' }
            ],
            imports: [BrowserModule,
                FormsModule,
                HttpModule,
                RouterModule,
                    RouterModule.forRoot([
                        { path: '', redirectTo: 'app', pathMatch: 'full' }
                    ])
                ]
        }).compileComponents();
    }));

    it('should do something', async(() => {
        TestBed.compileComponents().then(() => {
            const fixture = TestBed.createComponent(ComposersComponent);
            const app = fixture.componentInstance;
            app.ngOnInit();
            expect(app.composers).toEqual([
                { firstName: "Test", lastName: "Test", title: "Mr", awards: "None" },
                { firstName: "Test1", lastName: "Test1", title: "Mr", awards: "None"  },
            ]);
        });
    }));
});
