﻿using System.Collections.Generic;
using ANW.ComposerApp.Models;

public interface IComposersRepo
{
    IEnumerable<Composer> GetComposers();

    Composer GetComposer(int id);
}